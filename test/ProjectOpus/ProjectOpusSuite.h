/*
 * libSpiff - XSPF playlist handling library
 *
 * Copyright (C) 2006 Sebastian Pipping
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Sebastian Pipping, webmaster@hartwork.org
 */

#ifndef SPIFF_TEST_PROJECT_OPUS_SUITE_H
#define SPIFF_TEST_PROJECT_OPUS_SUITE_H 1


#include "../CppTest/cpptest.h"
#include <spiff/SpiffReaderCallback.h>
#include <spiff/SpiffDefines.h>


namespace Spiff {
	class SpiffReader;
	class SpiffTrack;
	class SpiffProps;
}


class ProjectOpusSuite : public Test::Suite, public Spiff::SpiffReaderCallback {

	std::basic_string<XML_Char> firstErrorText;
	int firstErrorLine;

public:
	ProjectOpusSuite();

private:
	void runCase(XML_Char const * filename, Spiff::SpiffReader & reader,
			int expectedCode);
	void parse_example();

	// SpiffReaderCallback
	void addTrack(Spiff::SpiffTrack * track);
	void setProps(Spiff::SpiffProps * props);
	bool handleError(int line, int column, int errorCode,
			XML_Char const * description);

};


#endif // SPIFF_TEST_PROJECT_OPUS_SUITE_H
