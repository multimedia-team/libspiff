/*
 * libSpiff - XSPF playlist handling library
 *
 * Copyright (C) 2006-2008, Sebastian Pipping / Xiph.Org Foundation
 * All rights reserved.
 *
 * Redistribution  and use in source and binary forms, with or without
 * modification,  are permitted provided that the following conditions
 * are met:
 *
 *     * Redistributions   of  source  code  must  retain  the   above
 *       copyright  notice, this list of conditions and the  following
 *       disclaimer.
 *
 *     * Redistributions  in  binary  form must  reproduce  the  above
 *       copyright  notice, this list of conditions and the  following
 *       disclaimer   in  the  documentation  and/or  other  materials
 *       provided with the distribution.
 *
 *     * Neither  the name of the Xiph.Org Foundation nor the names of
 *       its  contributors may be used to endorse or promote  products
 *       derived  from  this software without specific  prior  written
 *       permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS  IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT  NOT
 * LIMITED  TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND  FITNESS
 * FOR  A  PARTICULAR  PURPOSE ARE DISCLAIMED. IN NO EVENT  SHALL  THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL,    SPECIAL,   EXEMPLARY,   OR   CONSEQUENTIAL   DAMAGES
 * (INCLUDING,  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES;  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT  LIABILITY,  OR  TORT (INCLUDING  NEGLIGENCE  OR  OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Sebastian Pipping, sping@xiph.org
 */

/**
 * @file SpiffTrack.cpp
 * Implementation of SpiffTrack.
 */

#include <spiff/SpiffTrack.h>
#include <spiff/SpiffToolbox.h>
#include <cstring>

namespace Spiff {


/// @cond DOXYGEN_NON_API

/**
 * D object for SpiffTrack.
 */
class SpiffTrackPrivate {

	friend class SpiffTrack;

	XML_Char const * album; ///< Album
	bool ownAlbum; ///< Album memory ownership flag
	std::deque<std::pair<XML_Char const *, bool> *> * locations; ///< List of URI locations
	std::deque<std::pair<XML_Char const *, bool> *> * identifiers; ///< List of URI identifiers
	int trackNum; ///< Number of the track, must be positive
	int duration; ///< Duration in milliseconds, must be non-negative

	/**
	 * Creates a new D object.
	 */
	SpiffTrackPrivate()
			: album(NULL),
			ownAlbum(false),
			locations(NULL),
			identifiers(NULL),
			trackNum(-1),
			duration(-1) {

	}

	/**
	 * Copy constructor.
	 *
	 * @param source  Source to copy from
	 */
	SpiffTrackPrivate(SpiffTrackPrivate const & source)
			: album(source.ownAlbum
				? Toolbox::newAndCopy(source.album)
				: source.album),
			ownAlbum(source.ownAlbum),
			locations(NULL), // Created in copyDeque
			identifiers(NULL), // Created in copyDeque
			trackNum(source.trackNum),
			duration(source.duration) {
		if (source.locations != NULL) {
			copyDeque(this->locations, source.locations);
		}
		if (source.identifiers != NULL) {
			copyDeque(this->identifiers, source.identifiers);
		}
	}

	/**
	 * Assignment operator.
	 *
	 * @param source  Source to copy from
	 */
	SpiffTrackPrivate & operator=(SpiffTrackPrivate const & source) {
		if (this != &source) {
		    free();
            assign(source);
		}
		return *this;
	}

	/**
	 * Destroys this D object.
	 */
	~SpiffTrackPrivate() {
		free();
	}
	
	void free() {
		Toolbox::freeIfOwned(this->album, this->ownAlbum);
		if (this->locations != NULL) {
			freeDeque(this->locations);
		}
		if (this->identifiers != NULL) {
			freeDeque(this->identifiers);
		}
	}
	
	void assign(SpiffTrackPrivate const & source) {
        Toolbox::copyIfOwned(this->album, this->ownAlbum, source.album, source.ownAlbum);

		if (source.locations != NULL) {
			copyDeque(this->locations, source.locations);
		}

		if (source.identifiers != NULL) {
			copyDeque(this->identifiers, source.identifiers);
		}

		this->trackNum = source.trackNum;
		this->duration = source.duration;
	}

	static void freeDeque(std::deque<
			std::pair<XML_Char const *, bool> *> * & container) {
		std::deque<std::pair<XML_Char const *, bool> *>::const_iterator
				iter = container->begin();
		while (iter != container->end()) {
			std::pair<XML_Char const *, bool> * const entry = *iter;
			if (entry->second) {
				delete [] entry->first;
			}
			delete entry;
			iter++;
		}
		container->clear();
		delete container;
		container = NULL;
	}

	static void copyDeque(std::deque<
			std::pair<XML_Char const *, bool> *> * & dest,
			const std::deque<
			std::pair<XML_Char const *, bool> *> * source) {
		std::deque<std::pair<XML_Char const *, bool> *>::const_iterator
				iter = source->begin();
		while (iter != source->end()) {
			std::pair<XML_Char const *, bool> * const entry = *iter;

			bool const ownership = entry->second;
			XML_Char const * const value = ownership
					? Toolbox::newAndCopy(entry->first)
					: entry->first;
			SpiffTrack::appendHelper(dest, value, ownership);

			iter++;
		}
	}

};

/// @endcond


SpiffTrack::SpiffTrack()
		: SpiffData(),
		d(new SpiffTrackPrivate()) {

}


SpiffTrack::SpiffTrack(SpiffTrack const & source)
		: SpiffData(source),
		d(new SpiffTrackPrivate(*(source.d))) {

}


SpiffTrack & SpiffTrack::operator=(SpiffTrack const & source) {
	if (this != &source) {
		SpiffData::operator=(source);
		*(this->d) = *(source.d);
	}
	return *this;
}


SpiffTrack::~SpiffTrack() {
	delete this->d;
}


void SpiffTrack::giveAlbum(XML_Char const * album, bool copy) {
	Toolbox::deleteNewAndCopy(this->d->album, this->d->ownAlbum, album, copy);
}


void SpiffTrack::lendAlbum(XML_Char const * album) {
	Toolbox::deleteNewAndCopy(this->d->album, this->d->ownAlbum, album, false);
}


void SpiffTrack::setTrackNum(int trackNum) {
	this->d->trackNum = trackNum;
}


void SpiffTrack::setDuration(int duration) {
	this->d->duration = duration;
}


int SpiffTrack::getTrackNum() const {
	return this->d->trackNum;
}


int SpiffTrack::getDuration() const {
	return this->d->duration;
}


XML_Char * SpiffTrack::stealAlbum() {
	return SpiffData::stealHelper(this->d->album, this->d->ownAlbum);
}


XML_Char const * SpiffTrack::getAlbum() const {
	return this->d->album;
}


void SpiffTrack::giveAppendIdentifier(XML_Char const * identifier, bool copy) {
	appendHelper(this->d->identifiers, copy
			? Toolbox::newAndCopy(identifier)
			: identifier,
			true);
}


void SpiffTrack::giveAppendLocation(XML_Char const * location, bool copy) {
	appendHelper(this->d->locations, copy
			? Toolbox::newAndCopy(location)
			: location,
			true);
}


void SpiffTrack::lendAppendIdentifier(XML_Char const * identifier) {
	appendHelper(this->d->identifiers, identifier, false);
}


void SpiffTrack::lendAppendLocation(XML_Char const * location) {
	appendHelper(this->d->locations, location, false);
}


XML_Char * SpiffTrack::stealFirstIdentifier() {
	return stealFirstHelper(this->d->identifiers);
}


XML_Char * SpiffTrack::stealFirstLocation() {
	return stealFirstHelper(this->d->locations);
}


XML_Char const * SpiffTrack::getIdentifier(int index) const {
	return getHelper(this->d->identifiers, index);
}


XML_Char const * SpiffTrack::getLocation(int index) const {
	return getHelper(this->d->locations, index);
}


int SpiffTrack::getIdentifierCount() const {
	return (this->d->identifiers == NULL) ? 0 : static_cast<int>(this->d->identifiers->size());
}


int SpiffTrack::getLocationCount() const {
	return (this->d->locations == NULL) ? 0 : static_cast<int>(this->d->locations->size());
}


/*static*/ void SpiffTrack::appendHelper(
		std::deque<std::pair<XML_Char const *, bool> *> * & container,
		XML_Char const * value, bool ownership) {
	if (container == NULL) {
		container = new std::deque<std::pair<XML_Char const *, bool> *>;
	}
	std::pair<XML_Char const *, bool> * const entry =
			new std::pair<XML_Char const *, bool>(value, ownership);
	container->push_back(entry);
}


/*static*/ XML_Char * SpiffTrack::stealFirstHelper(
		std::deque<std::pair<XML_Char const *, bool> *> * & container) {
	if ((container == NULL) || container->empty()) {
		return NULL;
	}
	std::pair<XML_Char const *, bool> * const entry = container->front();
	container->pop_front();
	XML_Char * const res = entry->second
			? const_cast<XML_Char *>(entry->first)
			: Toolbox::newAndCopy(entry->first);
	delete entry;
	return res;
}


/*static*/ XML_Char const * SpiffTrack::getHelper(
		std::deque<std::pair<XML_Char const *, bool> *> * & container,
		int index) {
	if ((container == NULL) || container->empty() || (index < 0)
			|| (index >= static_cast<int>(container->size()))) {
		return NULL;
	}
	std::pair<XML_Char const *, bool> * const entry = container->at(index);

	// NOTE: getX() just peeps at data so don't clone anything
	return entry->first;
}


}
