/*
 * libSpiff - XSPF playlist handling library
 *
 * Copyright (C) 2006-2008, Sebastian Pipping / Xiph.Org Foundation
 * All rights reserved.
 *
 * Redistribution  and use in source and binary forms, with or without
 * modification,  are permitted provided that the following conditions
 * are met:
 *
 *     * Redistributions   of  source  code  must  retain  the   above
 *       copyright  notice, this list of conditions and the  following
 *       disclaimer.
 *
 *     * Redistributions  in  binary  form must  reproduce  the  above
 *       copyright  notice, this list of conditions and the  following
 *       disclaimer   in  the  documentation  and/or  other  materials
 *       provided with the distribution.
 *
 *     * Neither  the name of the Xiph.Org Foundation nor the names of
 *       its  contributors may be used to endorse or promote  products
 *       derived  from  this software without specific  prior  written
 *       permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS  IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT  NOT
 * LIMITED  TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND  FITNESS
 * FOR  A  PARTICULAR  PURPOSE ARE DISCLAIMED. IN NO EVENT  SHALL  THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL,    SPECIAL,   EXEMPLARY,   OR   CONSEQUENTIAL   DAMAGES
 * (INCLUDING,  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES;  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT  LIABILITY,  OR  TORT (INCLUDING  NEGLIGENCE  OR  OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Sebastian Pipping, sping@xiph.org
 */

/**
 * @file SpiffExtensionReaderFactory.cpp
 * Implementation of SpiffExtensionReaderFactory.
 */

#include <spiff/SpiffExtensionReaderFactory.h>
#include <spiff/SpiffExtensionReader.h>
#include <spiff/SpiffToolbox.h>

namespace Spiff {


/// @cond DOXYGEN_NON_API

/**
 * D object for SpiffExtensionReaderFactory.
 */
class SpiffExtensionReaderFactoryPrivate {

	friend class SpiffExtensionReaderFactory;

	///< Map of extension readers for playlist extensions
	std::map<XML_Char const *, SpiffExtensionReader const *,
			Toolbox::SpiffStringCompare> playlistExtensionReaders;

	///< Map of extension readers for track extensions
	std::map<XML_Char const *, SpiffExtensionReader const *,
			Toolbox::SpiffStringCompare> trackExtensionReaders; 

	///< Catch-all extension reader for playlist extensions
	SpiffExtensionReader const * playlistCatchAllReader;

	///< Catch-all extension reader for track extensions
	SpiffExtensionReader const * trackCatchAllReader; 

	/**
	 * Copies the extension reader and URI clones kept in a container
	 * over to another container.
	 * NOTE: The destination container is <strong>not</strong>
	 * cleared before.
	 *
	 * @param dest    Container to copy into
	 * @param source  Container to copy from
	 */
	static void copyMap(std::map<XML_Char const *, SpiffExtensionReader const *,
			Toolbox::SpiffStringCompare> & dest,
			const std::map<XML_Char const *, SpiffExtensionReader const *,
			Toolbox::SpiffStringCompare> & source) {
		// Copy examples and application URIs
		std::map<XML_Char const *, SpiffExtensionReader const *,
				Toolbox::SpiffStringCompare>
				::const_iterator iter = source.begin();
		while (iter != source.end()) {
			XML_Char const * const applicationUri
				= Toolbox::newAndCopy(iter->first);
			SpiffExtensionReader const * const clone
					= iter->second->createBrother();
			dest.insert(std::pair<XML_Char const *,
					SpiffExtensionReader const *>(applicationUri, clone));

			iter++;
		}
	}

	/**
	 * Deletes the extension reader and URI clones kept in a container.
	 * NOTE: The container itself is not deleted.
	 *
	 * @param container		Container to delete in
	 */
	static void freeMap(
			std::map<XML_Char const *, SpiffExtensionReader const *,
			Toolbox::SpiffStringCompare> & container) {
		// Free examples and application URIs
		std::map<XML_Char const *, SpiffExtensionReader const *,
				Toolbox::SpiffStringCompare>::iterator iter
				= container.begin();
		while (iter != container.end()) {
			delete [] iter->first;
			delete iter->second;
			iter++;
		}
	}

	/**
	 * Creates a new D object.
	 */
	SpiffExtensionReaderFactoryPrivate()
			: playlistCatchAllReader(NULL),
			trackCatchAllReader(NULL) {

	}

	/**
	 * Copy constructor.
	 *
	 * @param source  Source to copy from
	 */
	SpiffExtensionReaderFactoryPrivate(
			SpiffExtensionReaderFactoryPrivate const & source)
			: playlistCatchAllReader((source.playlistCatchAllReader == NULL)
				? NULL
				: source.playlistCatchAllReader->createBrother()),
			trackCatchAllReader((source.trackCatchAllReader == NULL)
				? NULL
				: source.trackCatchAllReader->createBrother()) {
		copyMap(this->playlistExtensionReaders, source.playlistExtensionReaders);
		copyMap(this->trackExtensionReaders, source.trackExtensionReaders);
	}

	/**
	 * Assignment operator.
	 *
	 * @param source  Source to copy from
	 */
	SpiffExtensionReaderFactoryPrivate & operator=(
			SpiffExtensionReaderFactoryPrivate const & source) {
		// playlistExtensionReaders
		freeMap(this->playlistExtensionReaders);
		this->playlistExtensionReaders.clear();
		copyMap(this->playlistExtensionReaders, source.playlistExtensionReaders);

		// trackExtensionReaders
		freeMap(this->trackExtensionReaders);
		this->trackExtensionReaders.clear();
		copyMap(this->trackExtensionReaders, source.trackExtensionReaders);

		// playlistCatchAllReader
		if (this->playlistCatchAllReader != NULL) {
			delete this->playlistCatchAllReader;
		}
		this->playlistCatchAllReader
				= (source.playlistCatchAllReader == NULL)
				? NULL
				: source.playlistCatchAllReader->createBrother();

		// trackCatchAllReader
		if (this->trackCatchAllReader != NULL) {
			delete this->trackCatchAllReader;
		}
		this->trackCatchAllReader
			= (source.trackCatchAllReader == NULL)
			? NULL
			: source.trackCatchAllReader->createBrother();
		return *this;
	}

	/**
	 * Destroys this D object.
	 */
	~SpiffExtensionReaderFactoryPrivate() {
		freeMap(this->playlistExtensionReaders);
		freeMap(this->trackExtensionReaders);
		if (this->playlistCatchAllReader != NULL) {
			delete this->playlistCatchAllReader;
		}
		if (this->trackCatchAllReader != NULL) {
			delete this->trackCatchAllReader;
		}
	}

};

/// @endcond


SpiffExtensionReaderFactory::SpiffExtensionReaderFactory()
		: d(new SpiffExtensionReaderFactoryPrivate()) {

}


SpiffExtensionReaderFactory::SpiffExtensionReaderFactory(
		SpiffExtensionReaderFactory const & source)
		: d(new SpiffExtensionReaderFactoryPrivate(*(source.d))) {

}


SpiffExtensionReaderFactory &
SpiffExtensionReaderFactory::operator=(
		SpiffExtensionReaderFactory const & source) {
	if (this != &source) {
		*(this->d) = *(source.d);
	}
	return *this;
}


SpiffExtensionReaderFactory::~SpiffExtensionReaderFactory() {
	delete this->d;
}


inline void
SpiffExtensionReaderFactory::registerReader(
		std::map<XML_Char const *, SpiffExtensionReader const *,
		Toolbox::SpiffStringCompare> & container, SpiffExtensionReader const * & catchAll,
		SpiffExtensionReader const * example,
		XML_Char const * triggerUri) {
	if (example == NULL) {
		return;
	}
	SpiffExtensionReader const * const clone = example->createBrother();

	// CatchAll reader?
	if (triggerUri == NULL) {
		// Overwrite old catcher
		if (catchAll != NULL) {
			delete catchAll;
		}
		catchAll = clone;
	} else {
		std::map<XML_Char const *, SpiffExtensionReader const *,
				Toolbox::SpiffStringCompare>
				::iterator found = container.find(triggerUri);
		if (found != container.end()) {
			// Overwrite existing entry
			delete found->second;
			found->second = clone;
		} else {
			// Add new entry with duped URI
			container.insert(std::pair<XML_Char const *,
					SpiffExtensionReader const *>(
					Toolbox::newAndCopy(triggerUri),
					clone));
		}
	}
}


void
SpiffExtensionReaderFactory::registerPlaylistExtensionReader(
		SpiffExtensionReader const * example,
		XML_Char const * triggerUri) {
	registerReader(this->d->playlistExtensionReaders,
			this->d->playlistCatchAllReader, example, triggerUri);
}


void
SpiffExtensionReaderFactory::registerTrackExtensionReader(
		SpiffExtensionReader const * example,
		XML_Char const * triggerUri) {
	registerReader(this->d->trackExtensionReaders,
			this->d->trackCatchAllReader, example, triggerUri);
}


inline void
SpiffExtensionReaderFactory::unregisterReader(
		std::map<XML_Char const *, SpiffExtensionReader const *,
		Toolbox::SpiffStringCompare> & container,
		SpiffExtensionReader const * & catchAll,
		XML_Char const * triggerUri) {
	// CatchAll reader?
	if (triggerUri == NULL) {
		// Remove catcher
		if (catchAll != NULL) {
			delete catchAll;
			catchAll = NULL;
		}
	} else {
		std::map<XML_Char const *, SpiffExtensionReader const *,
				Toolbox::SpiffStringCompare>
				::iterator found = container.find(triggerUri);
		if (found != container.end()) {
			delete found->second;
			container.erase(found);
		}
	}
}


void
SpiffExtensionReaderFactory::unregisterPlaylistExtensionReader(
		XML_Char const * triggerUri) {
	unregisterReader(this->d->playlistExtensionReaders,
			this->d->playlistCatchAllReader, triggerUri);
}


void
SpiffExtensionReaderFactory::unregisterTrackExtensionReader(
		XML_Char const * triggerUri) {
	unregisterReader(this->d->trackExtensionReaders,
			this->d->trackCatchAllReader, triggerUri);
}


inline SpiffExtensionReader *
SpiffExtensionReaderFactory::newReader(
		std::map<XML_Char const *, SpiffExtensionReader const *,
		Toolbox::SpiffStringCompare> & container,
		SpiffExtensionReader const * catchAll,
		XML_Char const * applicationUri,
		SpiffReader * reader) {
	std::map<XML_Char const *, SpiffExtensionReader const *,
			Toolbox::SpiffStringCompare>::const_iterator
			found = container.find(applicationUri);
	if (found != container.end()) {
		return found->second->createBrother(reader);
	} else {
		if (catchAll != NULL) {
			return catchAll->createBrother(reader);
		} else {
			return NULL;
		}
	}
}


SpiffExtensionReader *
SpiffExtensionReaderFactory::newPlaylistExtensionReader(XML_Char const * applicationUri,
		SpiffReader * reader) {
	return newReader(this->d->playlistExtensionReaders,
			this->d->playlistCatchAllReader, applicationUri, reader);
}


SpiffExtensionReader *
SpiffExtensionReaderFactory::newTrackExtensionReader(
		XML_Char const * applicationUri, SpiffReader * reader) {
	return newReader(this->d->trackExtensionReaders,
			this->d->trackCatchAllReader, applicationUri, reader);
}


} // namespace Spiff
