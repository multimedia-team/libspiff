/*
 * libSpiff - XSPF playlist handling library
 *
 * Copyright (C) 2006-2008, Sebastian Pipping / Xiph.Org Foundation
 * All rights reserved.
 *
 * Redistribution  and use in source and binary forms, with or without
 * modification,  are permitted provided that the following conditions
 * are met:
 *
 *     * Redistributions   of  source  code  must  retain  the   above
 *       copyright  notice, this list of conditions and the  following
 *       disclaimer.
 *
 *     * Redistributions  in  binary  form must  reproduce  the  above
 *       copyright  notice, this list of conditions and the  following
 *       disclaimer   in  the  documentation  and/or  other  materials
 *       provided with the distribution.
 *
 *     * Neither  the name of the Xiph.Org Foundation nor the names of
 *       its  contributors may be used to endorse or promote  products
 *       derived  from  this software without specific  prior  written
 *       permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS  IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT  NOT
 * LIMITED  TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND  FITNESS
 * FOR  A  PARTICULAR  PURPOSE ARE DISCLAIMED. IN NO EVENT  SHALL  THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL,    SPECIAL,   EXEMPLARY,   OR   CONSEQUENTIAL   DAMAGES
 * (INCLUDING,  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES;  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT  LIABILITY,  OR  TORT (INCLUDING  NEGLIGENCE  OR  OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Sebastian Pipping, sping@xiph.org
 */

/**
 * @file SpiffTrackWriter.h
 * Interface of SpiffTrackWriter.
 */

#ifndef SPIFF_TRACK_WRITER_H
#define SPIFF_TRACK_WRITER_H


#include "SpiffDataWriter.h"

namespace Spiff {


class SpiffXmlFormatter;
class SpiffTrack;
class SpiffWriter;
class SpiffTrackWriterPrivate;


/**
 * Writes a track to an XML formatter.
 */
class SpiffTrackWriter : public SpiffDataWriter {

private:
	/// @cond DOXYGEN_NON_API
	SpiffTrackWriterPrivate * const d; ///< D pointer
	/// @endcond

public:
	/**
	 * Creates a new track writer.
	 */
	SpiffTrackWriter();

	/**
	 * Copy constructor.
	 *
	 * @param source  Source to copy from
	 */
	SpiffTrackWriter(SpiffTrackWriter const & source);

	/**
	 * Assignment operator.
	 *
	 * @param source  Source to copy from
	 */
	SpiffTrackWriter & operator=(SpiffTrackWriter const & source);

	/**
	 * Destroys this track writer.
	 */
	~SpiffTrackWriter();

	/**
	 * Sets the track to write.
	 *
	 * @param track		Track to write
	 */
	void setTrack(SpiffTrack const * track);

protected:
	/**
	 * Initializes the track writer.
	 * Must be called before writing.
	 *
	 * @param output	Output formatter to write to
	 * @param version	XSPF version to produce
	 * @param baseUri	Absolute base URI to reduce against.
	 */
	void init(SpiffXmlFormatter & output, int version,
			XML_Char const * baseUri);

	/**
	 * Writes this track to the formatter.
	 */
	void write();

	/**
	 * Writes the album property.
	 */
	void writeAlbum();

	/**
	 * Writes the duration property.
	 */
	void writeDuration();

	/**
	 * Writes the list of identifiers.
	 */
	void writeIdentifiers();

	/**
	 * Writes the list of locations.
	 */
	void writeLocations();

	/**
	 * Writes the closing track tag.
	 */
	void writeTrackClose();

	/**
	 * Writes the opening track tag.
	 */
	void writeTrackOpen();

	/**
	 * Writes the trackNum property.
	 */
	void writeTrackNum();

	friend class SpiffWriter;

};


}

#endif // SPIFF_TRACK_WRITER_H
