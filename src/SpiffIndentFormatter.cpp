/*
 * libSpiff - XSPF playlist handling library
 *
 * Copyright (C) 2006-2008, Sebastian Pipping / Xiph.Org Foundation
 * All rights reserved.
 *
 * Redistribution  and use in source and binary forms, with or without
 * modification,  are permitted provided that the following conditions
 * are met:
 *
 *     * Redistributions   of  source  code  must  retain  the   above
 *       copyright  notice, this list of conditions and the  following
 *       disclaimer.
 *
 *     * Redistributions  in  binary  form must  reproduce  the  above
 *       copyright  notice, this list of conditions and the  following
 *       disclaimer   in  the  documentation  and/or  other  materials
 *       provided with the distribution.
 *
 *     * Neither  the name of the Xiph.Org Foundation nor the names of
 *       its  contributors may be used to endorse or promote  products
 *       derived  from  this software without specific  prior  written
 *       permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS  IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT  NOT
 * LIMITED  TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND  FITNESS
 * FOR  A  PARTICULAR  PURPOSE ARE DISCLAIMED. IN NO EVENT  SHALL  THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL,    SPECIAL,   EXEMPLARY,   OR   CONSEQUENTIAL   DAMAGES
 * (INCLUDING,  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES;  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT  LIABILITY,  OR  TORT (INCLUDING  NEGLIGENCE  OR  OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Sebastian Pipping, sping@xiph.org
 */

/**
 * @file SpiffIndentFormatter.cpp
 * Implementation of SpiffIndentFormatter.
 */

#include <spiff/SpiffIndentFormatter.h>
#include <spiff/SpiffStack.h>

namespace Spiff {


/**
 * Specifies the type of basic XML unit.
 */
enum SpiffElemPos {
	SPIFF_ELEM_DUMMY, ///< Stack returns 0 if empty

	SPIFF_ELEM_START, ///< Opening tag
	SPIFF_ELEM_BODY ///< Tag body
};


/// @cond DOXYGEN_NON_API

/**
 * D object for SpiffIndentFormatter.
 */
class SpiffIndentFormatterPrivate {

	friend class SpiffIndentFormatter;

	int level; ///< Element tree depth
	SpiffStack<unsigned int> stack; ///< Tag position stack
	int shift; ///< Indent shift

	/**
	 * Creates a new D object.
	 *
	 * @param shift          Non-positive indent shift (-2 will create two tabs less)
	 */
	SpiffIndentFormatterPrivate(int shift)
			: level(0), stack(), shift(shift) {

	}

	/**
	 * Copy constructor.
	 *
	 * @param source  Source to copy from
	 */
	SpiffIndentFormatterPrivate(const SpiffIndentFormatterPrivate &
			source)
			: level(source.level),
			stack(source.stack),
			shift(source.shift) {

	}

	/**
	 * Destroys this D object.
	 */
	~SpiffIndentFormatterPrivate() {
		this->stack.clear();
	}

};


/// @endcond

SpiffIndentFormatter::SpiffIndentFormatter(int shift)
		: SpiffXmlFormatter(),
		d(new SpiffIndentFormatterPrivate(shift)) {
	// Fix broken indent shift
	if (this->d->shift > 0) {
		this->d->shift = 0;
	}
}


SpiffIndentFormatter::SpiffIndentFormatter(SpiffIndentFormatter const & source)
		: SpiffXmlFormatter(source),
		d(new SpiffIndentFormatterPrivate(*(source.d))) {

}


SpiffIndentFormatter & SpiffIndentFormatter::operator=(SpiffIndentFormatter const & source) {
	if (this != &source) {
		SpiffXmlFormatter::operator=(source);
		*(this->d) = *(source.d);
	}
	return *this;
}


SpiffIndentFormatter::~SpiffIndentFormatter() {
	delete this->d;
}


void SpiffIndentFormatter::writeStart(XML_Char const * name,
		XML_Char const * const * atts) {
	// XML Header
	this->writeXmlDeclaration();
	*this->getOutput() << _PT("\n");

	// Indent
	for (int i = -(this->d->shift); i < this->d->level; i++) {
		*this->getOutput() << _PT('\t');
	}

	// Element
	*this->getOutput() << _PT('<') << name;
	while (atts[0] != NULL) {
		*this->getOutput() << _PT(' ') << atts[0] << _PT("=\"") << atts[1] << _PT("\"");
		atts += 2;
	}
	*this->getOutput() << _PT(">");

	this->d->level++;
	this->d->stack.push(SPIFF_ELEM_START);
}


void SpiffIndentFormatter::writeEnd(XML_Char const * name) {
	this->d->level--;
	if (this->d->stack.top() != SPIFF_ELEM_BODY) {
		*this->getOutput() << _PT('\n');
		for (int i = -(this->d->shift); i < this->d->level; i++) {
			*this->getOutput() << _PT('\t');
		}
	} else {
		// Pop body
		this->d->stack.pop();
	}
	// Pop start
	this->d->stack.pop();

	*this->getOutput() << _PT("</") << name << _PT('>');

	if (this->d->level == 0) {
		*this->getOutput() << _PT("\n");
	}
}


void SpiffIndentFormatter::writeBody(XML_Char const * text) {
	writeCharacterData(text);
	this->d->stack.push(SPIFF_ELEM_BODY);
}


void SpiffIndentFormatter::writeBody(int number) {
	*this->getOutput() << number;
	this->d->stack.push(SPIFF_ELEM_BODY);
}


}
