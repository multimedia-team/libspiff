/*
 * libSpiff - XSPF playlist handling library
 *
 * Copyright (C) 2006-2008, Sebastian Pipping / Xiph.Org Foundation
 * All rights reserved.
 *
 * Redistribution  and use in source and binary forms, with or without
 * modification,  are permitted provided that the following conditions
 * are met:
 *
 *     * Redistributions   of  source  code  must  retain  the   above
 *       copyright  notice, this list of conditions and the  following
 *       disclaimer.
 *
 *     * Redistributions  in  binary  form must  reproduce  the  above
 *       copyright  notice, this list of conditions and the  following
 *       disclaimer   in  the  documentation  and/or  other  materials
 *       provided with the distribution.
 *
 *     * Neither  the name of the Xiph.Org Foundation nor the names of
 *       its  contributors may be used to endorse or promote  products
 *       derived  from  this software without specific  prior  written
 *       permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS  IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT  NOT
 * LIMITED  TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND  FITNESS
 * FOR  A  PARTICULAR  PURPOSE ARE DISCLAIMED. IN NO EVENT  SHALL  THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL,    SPECIAL,   EXEMPLARY,   OR   CONSEQUENTIAL   DAMAGES
 * (INCLUDING,  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES;  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT  LIABILITY,  OR  TORT (INCLUDING  NEGLIGENCE  OR  OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Sebastian Pipping, sping@xiph.org
 */

/**
 * @file SpiffTrackWriter.cpp
 * Implementation of SpiffTrackWriter.
 */

#include "SpiffTrackWriter.h"
#include <spiff/SpiffTrack.h>
#include <spiff/SpiffXmlFormatter.h>
#include <spiff/SpiffExtension.h>
#include <spiff/SpiffExtensionWriter.h>
#include <cassert>

namespace Spiff {


/// @cond DOXYGEN_NON_API

/**
 * D object for SpiffTrackWriter.
 */
class SpiffTrackWriterPrivate {

	friend class SpiffTrackWriter;

private:
	SpiffTrack const * track; ///< Track to write
	int version; ///< XSPF version to produce

	/**
	 * Creates a new D object.
	 */
	SpiffTrackWriterPrivate() : track(NULL) {

	}

	/**
	 * Destroys this D object.
	 */
	~SpiffTrackWriterPrivate() {

	}

};

/// @endcond


SpiffTrackWriter::SpiffTrackWriter()
		: SpiffDataWriter(),
		d(new SpiffTrackWriterPrivate()) {

}


SpiffTrackWriter::SpiffTrackWriter(SpiffTrackWriter const & source)
		: SpiffDataWriter(source),
		d(new SpiffTrackWriterPrivate(*(source.d))) {

}


SpiffTrackWriter & SpiffTrackWriter::operator=(SpiffTrackWriter const & source) {
	if (this != &source) {
		SpiffDataWriter::operator=(source);
		*(this->d) = *(source.d);
	}
	return *this;
}


SpiffTrackWriter::~SpiffTrackWriter() {
	delete this->d;
}


void SpiffTrackWriter::setTrack(SpiffTrack const * track) {
	setData(track);
	this->d->track = track;
}


void
SpiffTrackWriter::init(SpiffXmlFormatter & output, int version,
		XML_Char const * baseUri) {
	this->getOutput() = &output;
	this->d->version = version;
	setBaseUri(baseUri);
}


void
SpiffTrackWriter::write() {
	writeTrackOpen();
	writeLocations();
	writeIdentifiers();
	writeTitle();
	writeCreator();
	writeAnnotation();
	writeInfo();
	writeImage();
	writeAlbum();
	writeTrackNum();
	writeDuration();
	writeLinks();
	writeMetas();
	if (this->d->version > 0) {
		writeExtensions();
	}
	writeTrackClose();
}


void SpiffTrackWriter::writeAlbum() {
	assert(this->d->track != NULL);
	XML_Char const * const album = this->d->track->getAlbum();
	if (album != NULL) {
		writePrimitive(_PT("album"), album);
	}
}


void SpiffTrackWriter::writeDuration() {
	assert(this->d->track != NULL);
	int const duration = this->d->track->getDuration();
	if (duration != -1) {
		writePrimitive(_PT("duration"), duration);
	}
}


void SpiffTrackWriter::writeTrackNum() {
	assert(this->d->track != NULL);
	int const trackNum = this->d->track->getTrackNum();
	if (trackNum != -1) {
		writePrimitive(_PT("trackNum"), trackNum);
	}
}


void SpiffTrackWriter::writeLocations() {
	assert(this->d->track != NULL);
	int index = 0;
	XML_Char const * location;
	for (;;) {
		location = this->d->track->getLocation(index++);
		if (location == NULL) {
			return;
		}

		XML_Char * const relUri = makeRelativeUri(location);
		writePrimitive(_PT("location"), relUri);
		delete [] relUri;
	}
}


void SpiffTrackWriter::writeIdentifiers() {
	assert(this->d->track != NULL);
	int index = 0;
	XML_Char const * identifier;
	for (;;) {
		identifier = this->d->track->getIdentifier(index++);
		if (identifier == NULL) {
			return;
		}

		XML_Char * const relUri = makeRelativeUri(identifier);
		writePrimitive(_PT("identifier"), relUri);
		delete [] relUri;
	}
}


void SpiffTrackWriter::writeTrackClose() {
	this->getOutput()->writeHomeEnd(_PT("track"));
}


void SpiffTrackWriter::writeTrackOpen() {
	XML_Char const * atts[1] = {NULL};
	this->getOutput()->writeHomeStart(_PT("track"), atts);
}


}
